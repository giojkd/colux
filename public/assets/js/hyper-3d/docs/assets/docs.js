


document.addEventListener( "DOMContentLoaded", () => {

	// file:// warning

	if ( location.protocol == 'file:' ) {
		document.body.classList.add('file-protocol');
	}


	// accordions

	for ( let element of document.querySelectorAll('h4') ) {
		element.addEventListener( 'click', toggleInfo );
	}


	function toggleInfo() {
		this.classList.toggle('open');
		return false;
	}
	
	
	// open anchor links
	
	if ( self.location.hash ) {
		let h4 = document.querySelector('h4'+self.location.hash);
		if ( h4 ) {
			openInfo(h4);
		}
	}
	
	for ( let element of document.querySelectorAll('a[href^="#"]') ) {
		element.addEventListener( 'click', function() {
			let h4 = document.querySelector('h4'+this.getAttribute('href'));
			openInfo( h4 );
		} );
	}
	
	function openInfo( h4 ) {
		if ( h4 && ! h4.classList.contains('open') ) {
			toggleInfo.bind(h4)();
			let parent = h4.closest('.info');
			if ( parent ) {
				let parent_h4 = parent.previousElementSibling;
				if ( ! parent_h4.classList.contains('open') ) {
					toggleInfo.bind(parent_h4)();
				}
			}
		}
	}
	
	
	// navi and index
	
	let nav = '<div class="page-index">';
	let index = 0;
	
	for ( let element of document.querySelectorAll('h2') ) {
		
		index++;
		
		element.id = 't' + index;
		nav += '<a href="#t'+ index +'" class="anchor-link">'+ element.innerHTML +'</a>';
		
	}
	
	nav += '</div>';
	
	let current_page = self.location.href.split('/').pop().split('#')[0].split('?')[0];
	let nav_item = document.querySelector('#index a[href*="'+current_page+'"]');
	nav_item.insertAdjacentHTML( 'afterend', nav);
	
	
	// live code
	
	for ( let element of document.querySelectorAll('.live-code') ) {
		
		if ( ! element.querySelector('.run-code') ) continue;
		
		element.querySelector('.run-code').addEventListener( 'click', function() {
			
			
			// remove example elements
			for ( let other_element of document.querySelectorAll('.show-output') ) {
				disposeElements( other_element );
			}			
			
			element.classList.add( 'show-output' );
			element.querySelector('.output').innerHTML = element.querySelector('textarea').value;
			
			// execute scripts
			for ( let script of element.querySelectorAll('script') ) {
				eval( script.textContent );
			}
			
		} );
		
		element.querySelector('.edit-code').addEventListener( 'click', function() {
			disposeElements( element );
		} );
		
	}
	
	
	function disposeElements( element ) {

		// free memory and webgl contexts
		for ( let comp of element.querySelectorAll('hyper-3d') ) {
			comp.dispose();
		}
		
		element.querySelector('.output').innerHTML = '';
		element.classList.remove( 'show-output' );
		
	}
	

} );

