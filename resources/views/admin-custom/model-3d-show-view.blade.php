@extends('vendor.backpack.base.layouts.top_left')
@section('content')



    <div class="row">
        <div class="col-md-4">
             <span id="result"></span>

    <div >
        <h3 class="text-white">Objs</h3>
        <ul id="objs-list" class="p-0" style="max-height: 480px; overflow-y: scroll">

        </ul>
        <button class="btn btn-primary mb-4">Add Obj</button>
    </div>

    <div >
        <h3 class="text-white">Materials</h3>
        <ul id="materials-list" class="p-0">

        </ul>
        <button class="btn btn-primary mb-4">Add Material</button>
    </div>

     <div >
        <h3 class="text-white">Textures</h3>
        <ul id="textures-list" class="p-0">

        </ul>
        <button class="btn btn-primary mb-4">Add Texture</button>
    </div>

    <form action="">
        <div class="form-group">
            <textarea class="form-control" name="" id="" cols="30" rows="5" placeholder="Description"></textarea>
        </div>
    </form>

    <button class="btn btn-success">Esporta</button>
        </div>
        <div class="col-md-8">
            <!--<hyper-3d debug id="model" grabbable="true" src="/gltf-models/glasses.fbx" high-quality grabbable="false" shadow="1" shadow-softness="1.6" autorotate-delay="4" autorotate-speed="-0.5" autorotate-elevation="10" zoom="2" elevation="10" ambient-light="1.4" point-light="0.25" point-light-position="0 1.75 0.5" playing></hyper-3d>-->


    <div class="bg-dark">
        <hyper-3d

        high-quality
        grabbable="true"
        shadow="1"
        shadow-softness="1.6"
        autorotate-delay="4"
        autorotate-speed="-0.5"
        autorotate-elevation="10"
        zoom="3"
        pivot="0"
        elevation="0"
        ambient-light="1.25"
        point-light="0"
        point-light-position="0 1.75 0.5"
        reflection="0.5"
        reflection-softness="2"
        camera-light="1.5"
        resource-path="/gltf-models/tex"
        id="model"
        zoomable
        src="/gltf-models/Room.gltf"
    ></hyper-3d>
    </div>
        </div>
    </div>

    @push('after_scripts')
        <script type="module" src="/assets/js/hyper-3d/hyper-3d.min.js"></script>

            <script>

            var selectedObject;

            var model = document.getElementById("model");

            function selectObj(name){
                var obj;
                obj = model.getObject(name);
                selectedObject = obj;
            }

            function setMaterialToSelectedObj(material){
                var materialObj = model.getMaterial(material);
                selectedObject.material = materialObj;
            }



            model.addEventListener( 'load', function(gltf){



                var materials = Array.from(this.getMaterials());
                console.log(materials);
                for(var i in materials){
                    console.log(materials[i]);
                    var m = materials[i];
                    var materialBtn = '<button class="btn btn-secondary mr-2 mb-2" onclick="setMaterialToSelectedObj(\''+m.name+'\')">'+m.name+'</button>'
                    $('#materials-list').append(materialBtn);
                }

                var children = [];

//                console.log(model.scene);

                model.scene.traverse( function ( child ) {
//                    console.log(child);
                    if ( child.isMesh ) {

                        if(child.name != ''){
                            children.push(child)
                            var objBtn = '<button class="btn btn-secondary mr-2 mb-2" onclick="selectObj(\''+child.name+'\')">'+child.name+'</button>'
                            $('#objs-list').append(objBtn);
                        }


                    }
                } );

                console.log(children);



            //var material = model.getObjectByName('Obj_000028').material;



                model.addEventListener( "click", function(event) {

                    selectedObject = this.raycast( event.clientX, event.clientY );


                    if ( selectedObject.name ) {
                        selectObj(selectedObject.name);
                    }

                } );
            })


            //model.getObject('Obj_000028').material = model.getMaterial('CubeMaterial');

        </script>
    @endpush
@endsection
